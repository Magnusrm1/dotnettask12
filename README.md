One professor has many students, while a student has one professor.
One professor has many subjects and a subject has many professors.
One student has one score card and a score card has one student.