﻿namespace DotNetTask12
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.lbProfs = new System.Windows.Forms.ListBox();
            this.btnDeleteProf = new System.Windows.Forms.Button();
            this.btnUpdateProf = new System.Windows.Forms.Button();
            this.btnAddProf = new System.Windows.Forms.Button();
            this.lblSubject = new System.Windows.Forms.Label();
            this.tbProfLName = new System.Windows.Forms.TextBox();
            this.lblProfLName = new System.Windows.Forms.Label();
            this.tbProfName = new System.Windows.Forms.TextBox();
            this.lblProfFName = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.cbStudProf = new System.Windows.Forms.ComboBox();
            this.lblStudProf = new System.Windows.Forms.Label();
            this.nudAvgScore = new System.Windows.Forms.NumericUpDown();
            this.lbStuds = new System.Windows.Forms.ListBox();
            this.btnDeleteStud = new System.Windows.Forms.Button();
            this.btnUpdateStud = new System.Windows.Forms.Button();
            this.btnAddStud = new System.Windows.Forms.Button();
            this.lblAverageScore = new System.Windows.Forms.Label();
            this.tbStudLName = new System.Windows.Forms.TextBox();
            this.lblstudLName = new System.Windows.Forms.Label();
            this.tbStudFName = new System.Windows.Forms.TextBox();
            this.lblStudFName = new System.Windows.Forms.Label();
            this.label123 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.clbSubjects = new System.Windows.Forms.CheckedListBox();
            this.btnJson = new System.Windows.Forms.Button();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudAvgScore)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.clbSubjects);
            this.panel1.Controls.Add(this.lbProfs);
            this.panel1.Controls.Add(this.btnDeleteProf);
            this.panel1.Controls.Add(this.btnUpdateProf);
            this.panel1.Controls.Add(this.btnAddProf);
            this.panel1.Controls.Add(this.lblSubject);
            this.panel1.Controls.Add(this.tbProfLName);
            this.panel1.Controls.Add(this.lblProfLName);
            this.panel1.Controls.Add(this.tbProfName);
            this.panel1.Controls.Add(this.lblProfFName);
            this.panel1.Location = new System.Drawing.Point(12, 12);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(487, 254);
            this.panel1.TabIndex = 0;
            // 
            // lbProfs
            // 
            this.lbProfs.FormattingEnabled = true;
            this.lbProfs.ItemHeight = 20;
            this.lbProfs.Location = new System.Drawing.Point(349, 18);
            this.lbProfs.Name = "lbProfs";
            this.lbProfs.Size = new System.Drawing.Size(120, 224);
            this.lbProfs.TabIndex = 9;
            this.lbProfs.SelectedIndexChanged += new System.EventHandler(this.lbProfs_SelectedIndexChanged);
            this.lbProfs.Format += new System.Windows.Forms.ListControlConvertEventHandler(this.LbProfsFormat);
            // 
            // btnDeleteProf
            // 
            this.btnDeleteProf.Location = new System.Drawing.Point(210, 200);
            this.btnDeleteProf.Name = "btnDeleteProf";
            this.btnDeleteProf.Size = new System.Drawing.Size(75, 42);
            this.btnDeleteProf.TabIndex = 8;
            this.btnDeleteProf.Text = "Delete";
            this.btnDeleteProf.UseVisualStyleBackColor = true;
            this.btnDeleteProf.Click += new System.EventHandler(this.btnDeleteProf_Click);
            // 
            // btnUpdateProf
            // 
            this.btnUpdateProf.Location = new System.Drawing.Point(129, 200);
            this.btnUpdateProf.Name = "btnUpdateProf";
            this.btnUpdateProf.Size = new System.Drawing.Size(75, 42);
            this.btnUpdateProf.TabIndex = 7;
            this.btnUpdateProf.Text = "Update";
            this.btnUpdateProf.UseVisualStyleBackColor = true;
            this.btnUpdateProf.Click += new System.EventHandler(this.btnUpdateProf_Click);
            // 
            // btnAddProf
            // 
            this.btnAddProf.Location = new System.Drawing.Point(45, 201);
            this.btnAddProf.Name = "btnAddProf";
            this.btnAddProf.Size = new System.Drawing.Size(75, 41);
            this.btnAddProf.TabIndex = 6;
            this.btnAddProf.Text = "Add";
            this.btnAddProf.UseVisualStyleBackColor = true;
            this.btnAddProf.Click += new System.EventHandler(this.btnAddProf_Click);
            // 
            // lblSubject
            // 
            this.lblSubject.AutoSize = true;
            this.lblSubject.Location = new System.Drawing.Point(44, 91);
            this.lblSubject.Name = "lblSubject";
            this.lblSubject.Size = new System.Drawing.Size(75, 20);
            this.lblSubject.TabIndex = 5;
            this.lblSubject.Text = "Subjects:";
            // 
            // tbProfLName
            // 
            this.tbProfLName.Location = new System.Drawing.Point(126, 47);
            this.tbProfLName.Name = "tbProfLName";
            this.tbProfLName.Size = new System.Drawing.Size(100, 26);
            this.tbProfLName.TabIndex = 3;
            // 
            // lblProfLName
            // 
            this.lblProfLName.AutoSize = true;
            this.lblProfLName.Location = new System.Drawing.Point(32, 47);
            this.lblProfLName.Name = "lblProfLName";
            this.lblProfLName.Size = new System.Drawing.Size(88, 20);
            this.lblProfLName.TabIndex = 2;
            this.lblProfLName.Text = "Last name:";
            // 
            // tbProfName
            // 
            this.tbProfName.Location = new System.Drawing.Point(126, 18);
            this.tbProfName.Name = "tbProfName";
            this.tbProfName.Size = new System.Drawing.Size(100, 26);
            this.tbProfName.TabIndex = 1;
            // 
            // lblProfFName
            // 
            this.lblProfFName.AutoSize = true;
            this.lblProfFName.Location = new System.Drawing.Point(32, 18);
            this.lblProfFName.Name = "lblProfFName";
            this.lblProfFName.Size = new System.Drawing.Size(88, 20);
            this.lblProfFName.TabIndex = 0;
            this.lblProfFName.Text = "First name:";
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.cbStudProf);
            this.panel2.Controls.Add(this.lblStudProf);
            this.panel2.Controls.Add(this.nudAvgScore);
            this.panel2.Controls.Add(this.lbStuds);
            this.panel2.Controls.Add(this.btnDeleteStud);
            this.panel2.Controls.Add(this.btnUpdateStud);
            this.panel2.Controls.Add(this.btnAddStud);
            this.panel2.Controls.Add(this.lblAverageScore);
            this.panel2.Controls.Add(this.tbStudLName);
            this.panel2.Controls.Add(this.lblstudLName);
            this.panel2.Controls.Add(this.tbStudFName);
            this.panel2.Controls.Add(this.lblStudFName);
            this.panel2.Location = new System.Drawing.Point(586, 12);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(487, 254);
            this.panel2.TabIndex = 10;
            // 
            // cbStudProf
            // 
            this.cbStudProf.FormattingEnabled = true;
            this.cbStudProf.Location = new System.Drawing.Point(126, 137);
            this.cbStudProf.Name = "cbStudProf";
            this.cbStudProf.Size = new System.Drawing.Size(121, 28);
            this.cbStudProf.TabIndex = 12;
            // 
            // lblStudProf
            // 
            this.lblStudProf.AutoSize = true;
            this.lblStudProf.Location = new System.Drawing.Point(41, 140);
            this.lblStudProf.Name = "lblStudProf";
            this.lblStudProf.Size = new System.Drawing.Size(81, 20);
            this.lblStudProf.TabIndex = 11;
            this.lblStudProf.Text = "Professor:";
            // 
            // nudAvgScore
            // 
            this.nudAvgScore.Location = new System.Drawing.Point(126, 92);
            this.nudAvgScore.Name = "nudAvgScore";
            this.nudAvgScore.Size = new System.Drawing.Size(100, 26);
            this.nudAvgScore.TabIndex = 10;
            // 
            // lbStuds
            // 
            this.lbStuds.FormattingEnabled = true;
            this.lbStuds.ItemHeight = 20;
            this.lbStuds.Location = new System.Drawing.Point(349, 18);
            this.lbStuds.Name = "lbStuds";
            this.lbStuds.Size = new System.Drawing.Size(120, 224);
            this.lbStuds.TabIndex = 9;
            this.lbStuds.SelectedIndexChanged += new System.EventHandler(this.lbStuds_SelectedIndexChanged);
            this.lbStuds.Format += new System.Windows.Forms.ListControlConvertEventHandler(this.LbStudsFormat);
            // 
            // btnDeleteStud
            // 
            this.btnDeleteStud.Location = new System.Drawing.Point(210, 200);
            this.btnDeleteStud.Name = "btnDeleteStud";
            this.btnDeleteStud.Size = new System.Drawing.Size(75, 42);
            this.btnDeleteStud.TabIndex = 8;
            this.btnDeleteStud.Text = "Delete";
            this.btnDeleteStud.UseVisualStyleBackColor = true;
            this.btnDeleteStud.Click += new System.EventHandler(this.btnDeleteStud_Click);
            // 
            // btnUpdateStud
            // 
            this.btnUpdateStud.Location = new System.Drawing.Point(129, 200);
            this.btnUpdateStud.Name = "btnUpdateStud";
            this.btnUpdateStud.Size = new System.Drawing.Size(75, 42);
            this.btnUpdateStud.TabIndex = 7;
            this.btnUpdateStud.Text = "Update";
            this.btnUpdateStud.UseVisualStyleBackColor = true;
            this.btnUpdateStud.Click += new System.EventHandler(this.btnUpdateStud_Click);
            // 
            // btnAddStud
            // 
            this.btnAddStud.Location = new System.Drawing.Point(45, 201);
            this.btnAddStud.Name = "btnAddStud";
            this.btnAddStud.Size = new System.Drawing.Size(75, 41);
            this.btnAddStud.TabIndex = 6;
            this.btnAddStud.Text = "Add";
            this.btnAddStud.UseVisualStyleBackColor = true;
            this.btnAddStud.Click += new System.EventHandler(this.btnAddStud_Click);
            // 
            // lblAverageScore
            // 
            this.lblAverageScore.AutoSize = true;
            this.lblAverageScore.Location = new System.Drawing.Point(5, 94);
            this.lblAverageScore.Name = "lblAverageScore";
            this.lblAverageScore.Size = new System.Drawing.Size(115, 20);
            this.lblAverageScore.TabIndex = 5;
            this.lblAverageScore.Text = "Average score:";
            // 
            // tbStudLName
            // 
            this.tbStudLName.Location = new System.Drawing.Point(126, 47);
            this.tbStudLName.Name = "tbStudLName";
            this.tbStudLName.Size = new System.Drawing.Size(100, 26);
            this.tbStudLName.TabIndex = 3;
            // 
            // lblstudLName
            // 
            this.lblstudLName.AutoSize = true;
            this.lblstudLName.Location = new System.Drawing.Point(32, 47);
            this.lblstudLName.Name = "lblstudLName";
            this.lblstudLName.Size = new System.Drawing.Size(88, 20);
            this.lblstudLName.TabIndex = 2;
            this.lblstudLName.Text = "Last name:";
            // 
            // tbStudFName
            // 
            this.tbStudFName.Location = new System.Drawing.Point(126, 18);
            this.tbStudFName.Name = "tbStudFName";
            this.tbStudFName.Size = new System.Drawing.Size(100, 26);
            this.tbStudFName.TabIndex = 1;
            // 
            // lblStudFName
            // 
            this.lblStudFName.AutoSize = true;
            this.lblStudFName.Location = new System.Drawing.Point(32, 18);
            this.lblStudFName.Name = "lblStudFName";
            this.lblStudFName.Size = new System.Drawing.Size(88, 20);
            this.lblStudFName.TabIndex = 0;
            this.lblStudFName.Text = "First name:";
            // 
            // label123
            // 
            this.label123.AutoSize = true;
            this.label123.Location = new System.Drawing.Point(164, 345);
            this.label123.Name = "label123";
            this.label123.Size = new System.Drawing.Size(85, 20);
            this.label123.TabIndex = 11;
            this.label123.Text = "Professors";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(856, 341);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(74, 20);
            this.label1.TabIndex = 12;
            this.label1.Text = "Students";
            // 
            // clbSubjects
            // 
            this.clbSubjects.CheckOnClick = true;
            this.clbSubjects.FormattingEnabled = true;
            this.clbSubjects.Location = new System.Drawing.Point(126, 91);
            this.clbSubjects.Name = "clbSubjects";
            this.clbSubjects.Size = new System.Drawing.Size(159, 96);
            this.clbSubjects.TabIndex = 10;
            // 
            // btnJson
            // 
            this.btnJson.Location = new System.Drawing.Point(138, 381);
            this.btnJson.Name = "btnJson";
            this.btnJson.Size = new System.Drawing.Size(174, 46);
            this.btnJson.TabIndex = 13;
            this.btnJson.Text = "Write json file";
            this.btnJson.UseVisualStyleBackColor = true;
            this.btnJson.Click += new System.EventHandler(this.btnJson_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1164, 450);
            this.Controls.Add(this.btnJson);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.label123);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudAvgScore)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TextBox tbProfName;
        private System.Windows.Forms.Label lblProfFName;
        private System.Windows.Forms.TextBox tbProfLName;
        private System.Windows.Forms.Label lblProfLName;
        private System.Windows.Forms.ListBox lbProfs;
        private System.Windows.Forms.Button btnDeleteProf;
        private System.Windows.Forms.Button btnUpdateProf;
        private System.Windows.Forms.Button btnAddProf;
        private System.Windows.Forms.Label lblSubject;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.ComboBox cbStudProf;
        private System.Windows.Forms.Label lblStudProf;
        private System.Windows.Forms.NumericUpDown nudAvgScore;
        private System.Windows.Forms.ListBox lbStuds;
        private System.Windows.Forms.Button btnDeleteStud;
        private System.Windows.Forms.Button btnUpdateStud;
        private System.Windows.Forms.Button btnAddStud;
        private System.Windows.Forms.Label lblAverageScore;
        private System.Windows.Forms.TextBox tbStudLName;
        private System.Windows.Forms.Label lblstudLName;
        private System.Windows.Forms.TextBox tbStudFName;
        private System.Windows.Forms.Label lblStudFName;
        private System.Windows.Forms.Label label123;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.CheckedListBox clbSubjects;
        private System.Windows.Forms.Button btnJson;
    }
}

