﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DotNetTask12.Model
{
    class Professor
    {
        // Id is the primary key.
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        // SubjectProfessors is the navigation object that stores all connections to
        // different subjects, where each SubjectProfessor contains foreign keys and
        // navigation objects to both a professor and a saubject.
        public ICollection<SubjectProfessor> SubjectProfessors { get; set; }
        // Navigation object used to get all students that has this professor in their 
        // ProfessorId and Professor property.
        public ICollection<Student> Students { get; set; }

        public override string ToString()
        {
            return FirstName + " " + LastName;
        }
    }
}
