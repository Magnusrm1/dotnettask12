﻿using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DotNetTask12.Model
{
    class MyDbContext : DbContext
    {
        public DbSet<Professor> Professors { get; set; }
        public DbSet<Student> Students { get; set; }
        public DbSet<Subject> Subjects { get; set; }
        public DbSet<SubjectProfessor> SubjectProfessors { get; set; }
        public DbSet<ScoreCard> ScoreCards { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder builder)
        {
            builder.UseSqlServer(System.Configuration.ConfigurationManager.ConnectionStrings["MyDb"].ConnectionString);
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<SubjectProfessor>().HasKey(pq => new { pq.SubjectId, pq.ProfessorId });

            modelBuilder.Entity<Subject>().HasData( new{ Id = 1, Name = "Math"});
            modelBuilder.Entity<Subject>().HasData( new { Id = 2, Name = "Algorithms" });
            modelBuilder.Entity<Subject>().HasData( new { Id = 3, Name = "Machine Learning" });

        }

        public void SerializeProfessorsToJSON()
        {
            var allProfessors = Professors
                .Include(p => p.Students)
                .ThenInclude(s => s.ScoreCard)
                .Include(p => p.SubjectProfessors)
                .ThenInclude(sp => sp.Subject);
            //.Include(p => p.SubjectProfessors);
            string json = JsonConvert.SerializeObject(allProfessors, Formatting.Indented, new JsonSerializerSettings() { 
            ReferenceLoopHandling = ReferenceLoopHandling.Ignore});

            //using (StreamWriter writer = new StreamWriter("professor_json.json"))
            File.WriteAllText("professor_json.json", json);
        }
    }
}
