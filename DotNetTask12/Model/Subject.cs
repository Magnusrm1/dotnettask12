﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DotNetTask12.Model
{
    class Subject
    {
        // Id is the primary key.
        public int Id { get; set; }
        public string Name { get; set; }
        // SubjectProfessors is a navigation property where all connections between 
        // this subject and professors are stored, where each SubjectProfessor contains
        // foreign keys and navigation objects to both a professor and a saubject.
        public ICollection<SubjectProfessor> SubjectProfessors { get; set; }

    }
}
