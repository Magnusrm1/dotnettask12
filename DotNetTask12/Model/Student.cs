﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DotNetTask12.Model
{
    class Student
    {
        // Id is the primary key.
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        // Scorecard is a navigation object to navigate to the students only ScoreCard.
        public ScoreCard ScoreCard { get; set; }
        // ProfessorId is the foreign key that connects the student to a professor.
        public int ProfessorId { get; set; }
        // Professor is the navigation property to professor from student.
        public Professor Professor { get; set; }

        public override string ToString()
        {
            return FirstName + " " + LastName;
        }
    }
}
