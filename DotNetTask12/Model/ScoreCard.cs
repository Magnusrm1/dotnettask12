﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DotNetTask12.Model
{
    class ScoreCard
    {
        // Id is the primary key.
        public int Id { get; set; }
        public int AverageScore { get; set; }
        // StudentId is the foreign key that connects the student to a professor. 
        // Setting this while the Student has a navigation property to a ScoreCard
        // signifies a one to one relationship, where scorecard is dependant on student.
        public int StudentId { get; set; }
        // Student is the navigation property to professor from student.
        public Student Student { get; set; }
    }
}
