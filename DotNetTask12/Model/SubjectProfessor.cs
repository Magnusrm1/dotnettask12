﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DotNetTask12.Model
{
    class SubjectProfessor
    {
        // Foreign key referring to a Subject. Acts as primary key together with ProfessorId.
        public int SubjectId { get; set; }
        // Navgation property to the relations subject object.
        public Subject Subject { get; set; }
        // Foreign key referring to a Professor. Acts as primary key together with SubjectId.
        public int ProfessorId { get; set; }
        // Navgation property to the relations professor object.
        public Professor Professor { get; set; }
    }
}
