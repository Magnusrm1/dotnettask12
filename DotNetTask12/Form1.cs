﻿using DotNetTask12.Model;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IdentityModel.Tokens;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DotNetTask12
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void btnAddProf_Click(object sender, EventArgs e)
        {
            // Add professor to database.
            
            using (MyDbContext db = new MyDbContext())
            {
                Professor prof = new Professor() { FirstName = tbProfName.Text, LastName = tbProfLName.Text, SubjectProfessors = new List<SubjectProfessor>() };
                db.Professors
                    .Add(prof);
                // Adding subjects
                List<Subject> checkedSubs = clbSubjects.CheckedItems.OfType<Subject>().ToList();
                foreach (Subject subject in checkedSubs)
                {
                    prof.SubjectProfessors
                        .Add(new SubjectProfessor() { Professor = prof, SubjectId = subject.Id});
                }
                
                db.SaveChanges();
            }
            Form1_Load(this, e);
        }

        private void btnUpdateProf_Click(object sender, EventArgs e)
        {
            // Update selected professor with current input values.
            using (MyDbContext db = new MyDbContext())
            {
                Professor chosenProf = db.Professors
                    .Where(prof => prof.Id == ((Professor)lbProfs.SelectedItem).Id)
                    .ToList()[0];
                chosenProf.FirstName = tbProfName.Text;
                chosenProf.LastName = tbProfLName.Text;
                // Initialize subject list if not initialized.
                if (chosenProf.SubjectProfessors is null)
                {
                    chosenProf.SubjectProfessors = new List<SubjectProfessor>();
                }
                // Remaking list of professors subjects.
                chosenProf.SubjectProfessors.Clear();
                List<Subject> checkedSubs = clbSubjects.CheckedItems.OfType<Subject>().ToList();
                foreach (Subject subject in checkedSubs)
                {
                    chosenProf.SubjectProfessors
                        .Add(new SubjectProfessor() { ProfessorId = chosenProf.Id, SubjectId = subject.Id });
                }
                db.SaveChanges();
            }
            Form1_Load(this, e);
        }

        private void btnDeleteProf_Click(object sender, EventArgs e)
        {
            // Delete selected professor from database.
            using (MyDbContext db = new MyDbContext())
            {
                Professor chosenProf = db.Professors
                    .Where(prof => prof.Id == ((Professor)lbProfs.SelectedItem).Id)
                    .ToList()[0];
                db.Professors.Remove(chosenProf);
                db.SaveChanges();
            }
            Form1_Load(this, e);
        }

        private void btnAddStud_Click(object sender, EventArgs e)
        {
            // Create the students scorecard.
            ScoreCard sc = new ScoreCard() { AverageScore = (int)nudAvgScore.Value };
            // Add student to database.
            Student stud = new Student() { FirstName = tbStudFName.Text, LastName = tbStudLName.Text, ProfessorId = ((Professor)cbStudProf.SelectedItem).Id, ScoreCard = sc };
            using (MyDbContext db = new MyDbContext())
            {
                db.Students.Add(stud);
                db.SaveChanges();
            }
            Form1_Load(this, e);
        }

        private void btnUpdateStud_Click(object sender, EventArgs e)
        {
            // Update selected student with current input values.
            using (MyDbContext db = new MyDbContext())
            {
                Student chosenStud = db.Students
                    .Where(stud => stud.Id == ((Student)lbStuds.SelectedItem).Id)
                    .ToList()[0];
                ScoreCard score = db.ScoreCards
                    .Where(sc => sc.StudentId == chosenStud.Id)
                    .ToList()[0];
                chosenStud.FirstName = tbStudFName.Text;
                chosenStud.LastName = tbStudLName.Text;
                score.AverageScore = Convert.ToInt32(nudAvgScore.Value);
                db.SaveChanges();
            }
            Form1_Load(this, e);
        }

        private void btnDeleteStud_Click(object sender, EventArgs e)
        {
            // Delete student from database.
            using (MyDbContext db = new MyDbContext())
            {
                Student chosenStud = db.Students
                    .Where(stud => stud.Id == ((Student)lbStuds.SelectedItem).Id)
                    .ToList()[0];
                db.Students.Remove(chosenStud);
                db.SaveChanges();
            }
            Form1_Load(this, e);
        }
        
        private void lbProfs_SelectedIndexChanged(object sender, EventArgs e)
        {
            using (MyDbContext db = new MyDbContext())
            {
                // Load selected professor to form.
                Professor prof = ((Professor)lbProfs.SelectedItem);
            
                var subjectProf = db.SubjectProfessors
                    .Where(sb => sb.ProfessorId == prof.Id)
                    .Include(sb => sb.Subject)
                    .Select(sb => sb.Subject).ToList();
                
                // Show current values.
                tbProfName.Text = prof.FirstName;
                tbProfLName.Text = prof.LastName;

                // Initialize subject list if not initialized.
                if (prof.SubjectProfessors is null)
                {
                    prof.SubjectProfessors = new List<SubjectProfessor>();
                }
                // Remove previous checkmarks.
                List<Subject> subjects = clbSubjects.Items.OfType<Subject>().ToList();
                foreach (Subject subject in subjects)
                {
                    clbSubjects.SetItemChecked(clbSubjects.Items.IndexOf(subject), false);
                }
                // Set checkmarks for the professors subjects.
                foreach (Subject subject in subjects)
                {
                    foreach (Subject subProf in subjectProf)
                    {
                        if (subject.Id == subProf.Id)
                        {
                            clbSubjects.SetItemChecked(clbSubjects.Items.IndexOf(subject), true);
                        }
                    }
                }
            }
        }

        private void lbStuds_SelectedIndexChanged(object sender, EventArgs e)
        {
            // Load selected student to form.
            Student student = (Student)lbStuds.SelectedItem;
            tbStudFName.Text = student.FirstName;
            tbStudLName.Text = student.LastName;
            using (MyDbContext db = new MyDbContext())
            {
                int avgScore = db.ScoreCards
                    .Where(sc => sc.StudentId == student.Id)
                    .ToList()[0].AverageScore;
                nudAvgScore.Value = avgScore;

                var prof = db.Students
                    .Where(s => s.Id == student.Id)
                    .Select(s => s.Professor).ToList()[0];
                // Set the combobox to the students professor.
                foreach (Professor professor in cbStudProf.Items)
                {
                    if (professor.Id == prof.Id)
                    {
                        cbStudProf.SelectedItem = professor;
                    }
                }
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            using (MyDbContext db = new MyDbContext())
            {
                // Load subjects to clb.
                clbSubjects.Items.Clear();
                clbSubjects.ValueMember = "Key";
                clbSubjects.DisplayMember = "Name";
                
                List<Subject> subjects = db.Subjects.ToList();
                foreach (Subject subject in subjects)
                {
                    clbSubjects.Items.Add(subject);
                }
                
                // Load professors to cb.
                cbStudProf.ValueMember = "Key";
                cbStudProf.DisplayMember = "Value";
                cbStudProf.DataSource = new BindingSource(
                    db.Professors.ToList(), 
                    null);
                // Load professors to lb.
                lbProfs.DataSource = db.Professors.ToList();
                lbProfs.DisplayMember = "FirstName";
                // Load students to lb.
                lbStuds.DataSource = db.Students.ToList();
                lbStuds.DisplayMember = "FirstName";
            }

        }

        // Formating display value of professor list box
        private void LbProfsFormat(object sender, ListControlConvertEventArgs e)
        {
            string firstName = ((Professor)e.ListItem).FirstName;
            string lastName = ((Professor)e.ListItem).LastName;

            e.Value = firstName + " " + lastName;
        }

        // Formating display value of student list box
        private void LbStudsFormat(object sender, ListControlConvertEventArgs e)
        {
            string firstName = ((Student)e.ListItem).FirstName;
            string lastName = ((Student)e.ListItem).LastName;

            e.Value = firstName + " " + lastName;
        }

        private void btnJson_Click(object sender, EventArgs e)
        {
            using (MyDbContext db = new MyDbContext())
            {
                db.SerializeProfessorsToJSON();
            }
        }
    }
}
