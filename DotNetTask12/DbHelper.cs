﻿using DotNetTask12.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DotNetTask12
{
    class DbHelper
    {
        public Professor GetProfessor(Professor professor)
        {
            try
            {
                return null;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return null;
            }

        }
        public bool AddProfessor(Professor professor)
        {
            try
            {

                return true;
            } catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return false;
            }
            
        }

        public bool DeleteProfessor(Professor professor)
        {
            try
            {

                return true;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return false;
            }

        }

        public bool UpdateProfessor(Professor professor)
        {
            try
            {

                return true;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return false;
            }

        }

        public Student GetStudent(Professor professor)
        {
            try
            {

                return null;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return null;
            }

        }

        public bool AddStudent(Student student)
        {
            try
            {

                return true;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return false;
            }

        }

        public bool DeleteStudent(Student student)
        {
            try
            {

                return true;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return false;
            }

        }

        public bool UpdateStudent(Student student)
        {
            try
            {

                return true;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return false;
            }

        }
    }
}
